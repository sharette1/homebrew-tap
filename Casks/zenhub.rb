cask :v1 => 'zenhub' do
  version '1.0'
  sha256 'd21b03450c6efe9a3ef4f23d2542844b62efa68aedbe97453ac0fb7983629ddf'

  url 'https://homebrew.shr.st/zenhub/ZenHub-1.0.zip'
  name 'zenhub'
  homepage 'https://www.zenhub.io'
  license :closed

  app 'ZenHub.app'
end
