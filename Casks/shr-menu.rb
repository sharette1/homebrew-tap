cask :v1 => 'shr-menu' do
  version '1.4'
  sha256 'e25471f90e0f6f28ae87bab80a9bb9510bb707505560f7d545af3ace843ac32e'

  url 'https://homebrew.shr.st/shr-menu/shr-1.4.zip'
  name 'shr-menu'
  homepage 'https://github.com/sharette/shr-menu'
  license :closed

  app 'shr.app'
end
