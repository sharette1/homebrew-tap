class Shr < Formula
  desc 'Development toolbelt'
  homepage 'https://github.com/sharette/shr'
  depends_on 'git'
  depends_on 'mercurial'

  ### BEGIN RELEASE
  url 'https://homebrew.sharette.fr/shr/shr-20151213005251-darwin.tar.gz'
  sha256 '04559c5fa36dee483fff313b2f7840e6f982306d091a5f164b027a75511c021c'
  ### END RELEASE

  devel do
    ### BEGIN STAGING
    url 'https://homebrew.shr.st/shr/shr-20151213004107-darwin.tar.gz'
    sha256 '16c38cafef2337b9649039aeac7ca7f9b66fac6c6731799e3acf064998b2512c'
    ### END STAGING
  end

  def install
    bin.install 'shr'

    man.mkpath
    man1.install Dir['man/*.1']
  end

  test do
    system "#{bin}/shr", 'help'
  end
end
